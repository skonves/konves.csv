﻿using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Konves.Csv.Tests
{
	[TestClass]
	public class CsvStreamReaderTests
	{
		[TestMethod]
		public void TrailingLineBreak()
		{
			// Arrange
			string data = "asdf\n";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord = "asdf";

			// Act
			string actualRecord = reader.ReadField();

			// Assert
			Assert.AreEqual(expectedRecord, actualRecord);
			Assert.IsTrue(reader.EndOfRecord);
			Assert.IsTrue(reader.EndOfStream);
		}

		[TestMethod]
		public void TrailingSpace()
		{
			// Arrange
			string data = "asdf, ";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord1 = "asdf";
			string expectedRecord2 = " ";

			// Act
			string actualRecord1 = reader.ReadField();
			Assert.IsFalse(reader.EndOfRecord);

			string actualRecord2 = reader.ReadField();

			// Assert
			Assert.IsTrue(reader.EndOfRecord);
			Assert.IsTrue(reader.EndOfStream);
			Assert.AreEqual(expectedRecord1, actualRecord1);
			Assert.AreEqual(expectedRecord2, actualRecord2);
		}

		[TestMethod]
		public void UnreadTrailingDelimiter()
		{
			// Arrange
			string data = "asdf,";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord = "asdf";
			bool expectedEndOfRecord = false;
			bool expectedEndOfStream = false;

			// Act
			string actualRecord = reader.ReadField();
			bool actualEndOfRecord = reader.EndOfRecord;
			bool actualEndOfStream = reader.EndOfStream;

			// Assert
			Assert.AreEqual(expectedRecord, actualRecord);
			Assert.AreEqual(expectedEndOfRecord, actualEndOfRecord);
			Assert.AreEqual(expectedEndOfStream, actualEndOfStream);
		}

		[TestMethod]
		public void ReadTrailingDelimiter()
		{
			// Arrange
			string data = "asdf,";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord1 = "asdf";
			string expectedRecord2 = string.Empty;
			bool expectedEndOfRecord = true;
			bool expectedEndOfStream = true;

			// Act
			string actualRecord1 = reader.ReadField();
			string actualRecord2 = reader.ReadField();
			bool actualEndOfRecord = reader.EndOfRecord;
			bool actualEndOfStream = reader.EndOfStream;

			// Assert
			Assert.AreEqual(expectedRecord1, actualRecord1);
			Assert.AreEqual(expectedRecord2, actualRecord2);
			Assert.AreEqual(expectedEndOfRecord, actualEndOfRecord);
			Assert.AreEqual(expectedEndOfStream, actualEndOfStream);
		}

		[TestMethod]
		public void SingleSpaceLine()
		{
			// Arrange
			string data = "asdf,qwerty\n ";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord1 = "asdf";
			string expectedRecord2 = "qwerty";
			string expectedRecord3 = " ";

			// Act
			string actualRecord1 = reader.ReadField();
			Assert.IsFalse(reader.EndOfRecord);
			string actualRecord2 = reader.ReadField();
			Assert.IsTrue(reader.EndOfRecord);
			string actualRecord3 = reader.ReadField();
			Assert.IsTrue(reader.EndOfRecord);

			// Assert
			Assert.IsTrue(reader.EndOfStream);
			Assert.AreEqual(expectedRecord1, actualRecord1);
			Assert.AreEqual(expectedRecord2, actualRecord2);
			Assert.AreEqual(expectedRecord3, actualRecord3);
		}

		[TestMethod]
		public void UnescapedSpace()
		{
			// Arrange
			string data = "asdf, ,qwerty";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord1 = "asdf";
			string expectedRecord2 = " ";
			string expectedRecord3 = "qwerty";

			// Act
			string actualRecord1 = reader.ReadField();
			string actualRecord2 = reader.ReadField();
			string actualRecord3 = reader.ReadField();

			// Assert
			Assert.AreEqual(expectedRecord1, actualRecord1);
			Assert.AreEqual(expectedRecord2, actualRecord2);
			Assert.AreEqual(expectedRecord3, actualRecord3);
		}

		[TestMethod]
		public void ExtraWhitespace()
		{
			// Arrange
			string data = "asdf, \"wxyz\" ,qwerty";
			Encoding encoding = Encoding.UTF8;
			Stream stream = new MemoryStream(encoding.GetBytes(data));
			CsvStreamReader reader = new CsvStreamReader(stream, encoding);
			string expectedRecord1 = "asdf";
			string expectedRecord2 = "wxyz";
			string expectedRecord3 = "qwerty";

			// Act
			string actualRecord1 = reader.ReadField();
			string actualRecord2 = reader.ReadField();
			string actualRecord3 = reader.ReadField();

			// Assert
			Assert.AreEqual(expectedRecord1, actualRecord1);
			Assert.AreEqual(expectedRecord2, actualRecord2);
			Assert.AreEqual(expectedRecord3, actualRecord3);
		}
	}
}
